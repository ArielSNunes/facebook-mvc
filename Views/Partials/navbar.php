<nav class="navbar is-warning is-fixed-top" role="navigation" aria-label="main navigation">
	<div class="navbar-brand">
		<a href="#" class="navbar-item brand">Facebook</a>
		<a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false">
			<span aria-hidden="true"></span>
			<span aria-hidden="true"></span>
			<span aria-hidden="true"></span>
		</a>
	</div>
	<!-- div.navbar-brand -->
	<div class="navbar-menu">
		<div class="navbar-start">
			<a class="navbar-item">
				Home
			</a>
		</div>
		<!-- div.navbar-start -->
		<div class="navbar-end">
			<a class="navbar-item">
				Home
			</a>
		</div>
		<!-- div.navbar-end -->
	</div>
	<!-- div.navbar-menu -->
</nav>
<!-- nav.navbar -->