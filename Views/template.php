<!DOCTYPE html>
<html lang="pt-br">

	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
		<meta http-equiv="X-UA-Compatible" content="ie=edge" />
		<title><?= APP_NAME ?></title>
		<link rel="stylesheet" href="<?= BASE_URL ?>/assets/css/reset.css"/>
		<link rel="stylesheet" href="<?= BASE_URL ?>/assets/css/bulma.min.css"/>
		<link rel="stylesheet" href="<?= BASE_URL ?>/assets/css/globals.css"/>
		<link rel="stylesheet" href="<?= BASE_URL ?>/assets/css/styles.css"/>
		<link rel="stylesheet" href="<?= BASE_URL ?>/assets/css/resp.css"/>
	</head>

	<body class="has-navbar-fixed-top">

		<?php $this->loadView('Partials/navbar', $viewData); ?>

		<?php $this->loadView($viewName, $viewData); ?>
		
		<script src="<?= BASE_URL ?>/assets/js/jquery.min.js"></script>
		<script src="<?= BASE_URL ?>/assets/js/app.js"></script>
	</body>

</html>