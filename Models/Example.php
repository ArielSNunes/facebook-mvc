<?php
namespace Models;

use Core\Model;

class Example extends Model
{
	private static $instance;
	public static function getInstance()
	{
		if (is_null(self::$instance))
			self::$instance = new self();
		return self::$instance;
	}
}