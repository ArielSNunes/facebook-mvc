<?php
namespace Core;

class Controller
{
	public function __construct()
	{
	}
	public function loadView($viewName, $viewData = array())
	{
		extract($viewData);
		require('Views' . DIRECTORY_SEPARATOR . $viewName . '.php');
	}
	public function loadTemplate($viewName, $viewData = array())
	{
		require('Views' . DIRECTORY_SEPARATOR . 'template.php');
	}
}