-- Arquivo para guardar os SQL que forem desenvolvidos, não é realmente usado no MVC
drop database if exists facebook;
create database facebook;
use facebook;

-- Tabelas
create table users (
	id int unsigned not null auto_increment,
	email varchar(150) not null unique,
	name varchar(100) not null,
	sex tinyint(1) not null,
	password varchar(32) not null,
	bio text,
	primary key (id)
) engine=InnoDB;

create table relationships (
	id int unsigned not null auto_increment,
	id_follower int unsigned not null,
	id_followed int unsigned not null,
	primary key (id)
) engine=InnoDB;

create table posts (
	id int unsigned not null auto_increment,
	id_user int unsigned not null,
	id_group int unsigned default 0,
	date_create datetime,
	type int unsigned not null,
	content text not null,
	primary key (id)
) engine=InnoDB;

create table posts_likes (
	id int unsigned not null auto_increment,
	id_post int unsigned not null,
	id_user int unsigned not null,
	primary key (id)
) engine=InnoDB;

create table posts_comments (
	id int unsigned not null auto_increment,
	id_post int unsigned not null,
	id_user int unsigned not null,
	date_comment datetime,
	content text not null,
	primary key (id)
) engine=InnoDB;

create table groups (
	id int unsigned not null auto_increment,
	id_user int unsigned not null,
	title varchar(150) not null,
	description text,
	primary key (id)
) engine=InnoDB;

create table groups_members (
	id int unsigned not null auto_increment,
	id_group int unsigned not null,
	id_user int unsigned not null,
	primary key (id)
) engine=InnoDB;

-- Relacionamentos
alter table relationships add constraint fk_relationships_id_follower foreign key (id_follower) references users(id);

alter table relationships add constraint fk_relationships_id_followed foreign key (id_followed) references users(id);

alter table posts add constraint fk_posts_id_user foreign key (id_user) references users(id);

alter table posts add constraint fk_posts_id_group foreign key (id_group) references groups(id);

alter table posts_likes add constraint fk_posts_likes_id_post foreign key (id_post) references posts(id);

alter table posts_likes add constraint fk_posts_likes_id_user foreign key (id_user) references users(id);

alter table posts_comments add constraint fk_posts_comments_id_post foreign key (id_post) references posts(id);

alter table posts_comments add constraint fk_posts_comments_id_user foreign key (id_user) references users(id);

alter table groups add constraint fk_groups_id_user foreign key (id_user) references users(id);

alter table groups_members add constraint fk_groups_members_id_user foreign key (id_user) references users(id);

alter table groups_members add constraint fk_groups_members_id_group foreign key (id_group) references groups(id);

-- Inserts