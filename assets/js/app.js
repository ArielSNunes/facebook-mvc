(function (win, doc, $) {
	$(document).ready(function () {
		$(".navbar-burger").click(function () {
			$(".navbar-burger").toggleClass("is-active");
			$(".navbar-menu").toggleClass("is-active");
		});
	});
})(window, document, jQuery);